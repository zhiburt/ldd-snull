CONFIG_MODULE_SIG=n

ifneq ($(KERNELRELEASE),)
	obj-m += snull.o
else
    KERNELDIR ?= /lib/modules/$(shell uname -r)/build
    PWD := $(shell pwd)
endif

.PHONY: all
all:
	make -C /lib/modules/$(shell uname -r)/build M=$(shell pwd) modules

.PHONY: all
clean:
	make -C /lib/modules/$(shell uname -r)/build M=$(shell pwd) clean
