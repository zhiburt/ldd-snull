#include <asm-generic/errno.h>
#include <asm/checksum.h>
#include <linux/errno.h>
#include <linux/etherdevice.h>
#include <linux/if.h>
#include <linux/if_ether.h>
#include <linux/in.h>
#include <linux/in6.h>
#include <linux/interrupt.h>
#include <linux/ip.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/netdevice.h>
#include <linux/sched.h>
#include <linux/skbuff.h>
#include <linux/slab.h>
#include <linux/tcp.h>
#include <linux/types.h>

#include "snull.h"

static int snull_init_void(struct net_device *dev);
static int snull_open(struct net_device *dev);
static int snull_close(struct net_device *dev);
static int snull_config(struct net_device *dev, struct ifmap *cfg);
static int snull_tx(struct sk_buff *skb, struct net_device *dev);
static int snull_ioctl(struct net_device *dev, struct ifreq *req, int cmd);
static struct net_device_stats *snull_stats(struct net_device *dev);
static int snull_change_mtu(struct net_device *dev, int new_mtu);
static void snull_tx_timeout(struct net_device *dev, unsigned int timeout);

static int snull_header(struct sk_buff *skb, struct net_device *dev,
                        unsigned short type, const void *dst_addr,
                        const void *src_addr, unsigned len);

static const struct net_device_ops snull_netdev_ops = {
    .ndo_init = snull_init_void,
    .ndo_open = snull_open,
    .ndo_stop = snull_close,
    .ndo_set_config = snull_config,
    .ndo_start_xmit = snull_tx,
    .ndo_do_ioctl = snull_ioctl,
    .ndo_get_stats = snull_stats,
    .ndo_change_mtu = snull_change_mtu,
    .ndo_tx_timeout = snull_tx_timeout,
};

static const struct header_ops snull_header_ops = {
    .create = snull_header,
    .cache = NULL,
};

/* PARAMS */
static int timeout = 0;
static int use_napi = 0;
static int pkt_pool_size = 8;

static struct net_device *snull_devs[2];

static int snull_init_void(struct net_device *dev) { return 0; }

static int snull_tx_init(struct net_device *dev) {
  struct snull_priv *priv;
  struct snull_packet *pkt;

  priv = netdev_priv(dev);

  priv->tx_queue = NULL;
  for (int i = 0; i < pkt_pool_size; i++) {
    pkt = kmalloc(sizeof(struct snull_packet), GFP_KERNEL);
    if (pkt == NULL) {
      return -1;
    }

    memset(pkt, 0, sizeof(struct snull_packet));

    pkt->dev = dev;
    pkt->next = priv->tx_queue;

    priv->tx_queue = pkt;
  }

  return 0;
}

static void snull_tx_free(struct net_device *dev) {
  struct snull_priv *priv;
  struct snull_packet *pkt;

  priv = netdev_priv(dev);

  while ((pkt = priv->tx_queue)) {
    kfree(pkt);
    priv->tx_queue = pkt->next;
  }
}

static struct snull_packet *snull_tx_dequeue(struct net_device *dev) {
  struct snull_priv *priv;
  struct snull_packet *pkt;
  unsigned long flags = 0;

  priv = netdev_priv(dev);

  spin_lock_irqsave(&priv->lock, flags);

  pkt = priv->tx_queue;
  priv->tx_queue = pkt->next;

  if (priv->tx_queue == NULL) {
    printk("snull: pkt pool empty\n");
    netif_stop_queue(dev);
  }

  spin_unlock_irqrestore(&priv->lock, flags);

  return pkt;
}

static void snull_tx_enqueue(struct snull_packet *pkt) {
  struct snull_priv *priv;
  unsigned long flags = 0;

  priv = netdev_priv(pkt->dev);

  spin_lock_irqsave(&priv->lock, flags);

  pkt->next = priv->tx_queue;
  priv->tx_queue = pkt;

  spin_unlock_irqrestore(&priv->lock, flags);

  if (netif_queue_stopped(pkt->dev) && pkt->next == NULL) {
    netif_wake_queue(pkt->dev);
  }
}

static void snull_rx_enqueue(struct net_device *dev, struct snull_packet *pkt) {
  struct snull_priv *priv;
  unsigned long flags = 0;

  priv = netdev_priv(pkt->dev);

  spin_lock_irqsave(&priv->lock, flags);

  pkt->next = priv->rx_queue;
  priv->rx_queue = pkt;

  spin_unlock_irqrestore(&priv->lock, flags);

  if (netif_queue_stopped(pkt->dev) && pkt->next == NULL) {
    netif_wake_queue(pkt->dev);
  }
}

static struct snull_packet *snull_rx_dequeue(struct net_device *dev) {
  unsigned long flags = 0;
  struct snull_priv *priv;
  struct snull_packet *pkt = NULL;

  priv = netdev_priv(pkt->dev);

  spin_lock_irqsave(&priv->lock, flags);

  pkt = priv->rx_queue;
  if (pkt != NULL) {
    priv->rx_queue = pkt->next;
  }

  spin_unlock_irqrestore(&priv->lock, flags);

  return pkt;
}

static int snull_open(struct net_device *dev) {
  printk(KERN_ALERT "snull: SNULL OPEN\n");

  netif_start_queue(dev);

  return 0;
}

static int snull_close(struct net_device *dev) {
  printk(KERN_ALERT "snull: SNULL CLOSE\n");

  netif_stop_queue(dev);

  return 0;
}

static int snull_config(struct net_device *dev, struct ifmap *cfg) {
  if (dev->flags & IFF_UP) {
    return -EBUSY;
  }

  if (cfg->base_addr != dev->base_addr) {
    printk(KERN_WARNING "snull: can't change I/O address\n");
    return -EOPNOTSUPP;
  }

  if (cfg->irq != dev->irq) {
    // note: arent we supposed to lock in while doing it?
    dev->irq = cfg->irq;
  }

  return 0;
}

static int snull_rx(struct net_device *dev, struct snull_packet *pkt) {
  struct snull_priv *priv;
  struct sk_buff *skb, *skb_tail;
  int err = 0;

  priv = netdev_priv(dev);

  skb = dev_alloc_skb(pkt->len + 2);
  if (!skb) {
    priv->stats.rx_dropped++;
    err = -ENOMEM;
    goto out;
  }

  skb_reserve(skb, 2);
  skb_tail = skb_put(skb, pkt->len);
  memcpy(skb_tail, pkt->data, pkt->len);

  skb->dev = dev;
  skb->protocol = eth_type_trans(skb, dev);
  skb->ip_summed = CHECKSUM_UNNECESSARY;

  priv->stats.rx_packets++;
  priv->stats.rx_bytes += pkt->len;

  netif_rx(skb);

out:
  return err;
}

static int snull_rx_poll(struct napi_struct *napi, int packet_limit) {
  struct snull_priv *priv;
  struct snull_packet *pkt;
  struct net_device *dev;
  struct sk_buff *skb, *skb_tail;
  int npackets = 0;

  priv = container_of(napi, struct snull_priv, napi);
  dev = priv->dev;

  while (npackets < packet_limit && priv->rx_queue) {
    pkt = snull_rx_dequeue(dev);

    skb = dev_alloc_skb(pkt->len + 2);
    if (!skb) {
      // note: we drop the packet but shall be move it to tx?

      priv->stats.rx_dropped++;
      snull_tx_enqueue(pkt);
      continue;
    }

    skb_reserve(skb, 2);
    skb_tail = skb_put(skb, pkt->len);
    memcpy(skb_tail, pkt->data, pkt->len);

    skb->dev = dev;
    skb->protocol = eth_type_trans(skb, dev);
    skb->ip_summed = CHECKSUM_UNNECESSARY;

    priv->stats.rx_packets++;
    priv->stats.rx_bytes += pkt->len;

    npackets++;

    netif_receive_skb(skb);

    snull_tx_enqueue(pkt);
  }

  if (!priv->rx_queue) {
    napi_complete(napi);
    priv->rx_int_enabled = 1;
  }

  return npackets;
}

static void snull_int_handler(int irq, void *devptr, struct regs *regs) {
  struct snull_priv *priv;
  struct snull_packet *pkt;
  struct net_device *dev;
  int status = 0;

  if (!devptr) {
    goto out;
  }

  dev = (struct net_device *)devptr;
  priv = netdev_priv(dev);

  spin_lock(&priv->lock);

  status = priv->status;
  priv->status = 0;

  if (status & SNULL_STATUS_RX_INTR) {
    pkt = priv->rx_queue;
    if (pkt) {
      priv->rx_queue = pkt->next;
      snull_rx(dev, pkt);
    }
  }

  if (status & SNULL_STATUS_TX_INTR) {
    priv->stats.tx_packets++;
    priv->stats.tx_bytes += priv->tx_packet_len;
    dev_kfree_skb(priv->skb);
  }

  spin_unlock(&priv->lock);

  // we do it here cause of an inner spin lock
  if (pkt) {
    snull_tx_enqueue(pkt);
  }

out:
  return;
}

static void snull_int_napi_handler(int irq, void *devptr, struct regs *regs) {
  struct snull_priv *priv;
  struct net_device *dev;
  int status = 0;

  if (!devptr) {
    goto out;
  }

  dev = (struct net_device *)devptr;
  priv = netdev_priv(dev);

  spin_lock(&priv->lock);

  status = priv->status;
  priv->status = 0;

  if (status & SNULL_STATUS_RX_INTR) {
    priv->rx_int_enabled = 0;
    napi_schedule(&priv->napi);
  }

  if (status & SNULL_STATUS_TX_INTR) {
    priv->stats.tx_packets++;
    priv->stats.tx_bytes += priv->tx_packet_len;
    dev_kfree_skb(priv->skb);
  }

  spin_unlock(&priv->lock);

out:
  return;
}

static void snull_hw_tx(struct net_device *dev, char *buf, int len) {
  struct snull_priv *priv;
  struct net_device *dst;
  struct snull_packet *tx_queue;
  struct iphdr *ih;
  u32 *src_addr, *dst_addr;
  int dev_kind;

  if (len < sizeof(struct ethhdr) + sizeof(struct iphdr)) {
    printk(KERN_WARNING "snull: an unknown packet recieved (%i octets)\n", len);
    goto out;
  }

  // are we OK with indianess here?
  ih = (struct iphdr *)(buf + sizeof(struct ethhdr));
  src_addr = &ih->saddr;
  dst_addr = &ih->daddr;

  // ip address tweak
  ((u8 *)src_addr)[2] ^= 1;
  ((u8 *)dst_addr)[2] ^= 1;

  ih->check = 0;
  ih->check = ip_fast_csum((unsigned char *)ih, ih->ihl);

  dev_kind = dev == snull_devs[0] ? 1 : 0;
  dst = snull_devs[dev_kind];

  priv = netdev_priv(dst);
  tx_queue = snull_tx_dequeue(dev);
  tx_queue->len = len;
  memcpy(tx_queue->data, buf, len);
  snull_rx_enqueue(dst, tx_queue);

  if (priv->rx_int_enabled) {
    priv->status |= SNULL_STATUS_RX_INTR;
    (*(priv->int_handler))(0, dst, NULL);
  }

  priv = netdev_priv(dev);
  priv->tx_packet_len = len;
  priv->tx_packet_data = buf;
  priv->status |= SNULL_STATUS_TX_INTR;

  (*(priv->int_handler))(0, dev, NULL);

out:
  return;
}

static int snull_tx(struct sk_buff *skb, struct net_device *dev) {
  struct snull_priv *priv;
  char shortpkt[ETH_ZLEN];
  char *data;
  int len;

  priv = netdev_priv(dev);
  len = skb->len;
  data = skb->data;

  if (len < ETH_ZLEN) {
    memset(shortpkt, 0, ETH_ZLEN);
    memcpy(shortpkt, skb->data, skb->len);
    len = ETH_ZLEN;
    data = shortpkt;
  }

  // is this correct?
  // there's netif_trans_update which does it by atomic
  dev->ingress_queue->trans_start = jiffies;
  priv->skb = skb;
  snull_hw_tx(dev, data, len);

  return 0;
}

static void snull_tx_timeout(struct net_device *dev, unsigned int timeout) {
  struct snull_priv *priv;

  priv = netdev_priv(dev);
  priv->status = SNULL_STATUS_TX_INTR;

  (*(priv->int_handler))(0, dev, NULL);

  priv->stats.tx_errors++;
  netif_wake_queue(dev);
}

static int snull_ioctl(struct net_device *dev, struct ifreq *req, int cmd) {
  return 0;
}

static struct net_device_stats *snull_stats(struct net_device *dev) {
  struct snull_priv *priv;

  priv = netdev_priv(dev);

  return &priv->stats;
}

static int snull_header(struct sk_buff *skb, struct net_device *dev,
                        unsigned short type, const void *dst_addr,
                        const void *src_addr, unsigned len) {
  struct ethhdr *eth;
  const void *src, *dst;
  int header_len;

  src = src_addr ? src_addr : dev->dev_addr;
  dst = dst_addr ? dst_addr : dev->dev_addr;

  eth = (struct ethhdr *)skb_push(skb, ETH_HLEN);
  eth->h_proto = htons(type);

  memcpy(eth->h_source, src, dev->addr_len);
  memcpy(eth->h_dest, dst, dev->addr_len);

  // change the ip
  eth->h_dest[ETH_ALEN + 1] ^= 0x01;

  header_len = dev->hard_header_len;

  return header_len;
}

static int snull_change_mtu(struct net_device *dev, int new_mtu) {
  struct snull_priv *priv;
  unsigned long flags;
  int err = 0;

  // max ethernet MTU
  if (new_mtu > 1500) {
    err = -EINVAL;
    goto out;
  }

  priv = netdev_priv(dev);
  spin_lock_irqsave(&priv->lock, flags);
  dev->mtu = new_mtu;
  spin_unlock_irqrestore(&priv->lock, flags);

out:
  return err;
}

void snull_priv_init(struct net_device *dev) {
  struct snull_priv *priv;

  priv = netdev_priv(dev);
  memset(priv, 0, sizeof(struct snull_priv));

  spin_lock_init(&priv->lock);
  priv->dev = dev;
  priv->rx_int_enabled = 1;

  if (use_napi) {
    priv->int_handler = (snull_int_t)(&snull_int_napi_handler);
  } else {
    priv->int_handler = (snull_int_t)(&snull_int_handler);
  }

  if (snull_tx_init(dev)) {
    printk(KERN_ALERT "snull: snull_tx_init error\n");
    return;
  }

  ether_setup(dev);

  dev->watchdog_timeo = timeout;
  if (use_napi) {
    netif_napi_add(dev, &priv->napi, snull_rx_poll);
  }

  dev->flags |= IFF_NOARP;
  dev->features |= NETIF_F_HW_CSUM;
  dev->netdev_ops = &snull_netdev_ops;
  dev->header_ops = &snull_header_ops;
}

static void snull_priv_cleanup(void) {
  struct snull_priv *priv;

  for (int i = 0; i < 2; i++) {
    if (snull_devs[i]) {
      priv = netdev_priv(snull_devs[i]);

      if (priv->is_registered) {
        snull_tx_free(snull_devs[i]);
        unregister_netdev(snull_devs[i]);
      }

      free_netdev(snull_devs[i]);
    }
  }
}

static int __init snull_mod_init(void) {
  struct snull_priv *priv;
  int err = 0;

  printk("snull: loaded\n");

  for (int i = 0; i < 2; i++) {
    snull_devs[i] = alloc_netdev(sizeof(struct snull_priv), "sn%d",
                                 NET_NAME_UNKNOWN, snull_priv_init);
    if (snull_devs[i] == NULL) {
      printk(KERN_ALERT "snull: alloc net_device error\n");
      err = -ENOMEM;
      goto alloc_free;
    }
  }

  printk(KERN_ALERT "snull: alloccated device '%p' '%s'\n", snull_devs[0],
         snull_devs[0]->name);
  printk(KERN_ALERT "snull: alloccated device '%p' '%s'\n", snull_devs[1],
         snull_devs[1]->name);

  for (int i = 0; i < 2; i++) {
    if (register_netdev(snull_devs[i])) {
      printk(KERN_ALERT "snull: failed to register device '%s'\n",
             snull_devs[i]->name);
      err = -ENODEV;
      goto alloc_free;
    }

    priv = netdev_priv(snull_devs[i]);
    priv->is_registered = 1;
  }

  goto out;

alloc_free:
  if (snull_devs[0] != NULL) {
    free_netdev(snull_devs[0]);
  }

  if (snull_devs[1] != NULL) {
    free_netdev(snull_devs[1]);
  }
out:

  printk("snull: loaded done\n");

  return err;
}

static void __exit snull_mod_exit(void) {
  printk("snull: unloaded\n");
  snull_priv_cleanup();
}

module_param(timeout, int, 0);
module_param(use_napi, int, 0);
module_param(pkt_pool_size, int, 0);

module_init(snull_mod_init);
module_exit(snull_mod_exit);

MODULE_LICENSE("Dual BSD/GPL");
MODULE_AUTHOR("LLD");
MODULE_DESCRIPTION("LDD snull example");
MODULE_VERSION("0.0.1");
