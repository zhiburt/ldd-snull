#ifndef __SNULL__H
#define __SNULL__H

#include <linux/netdevice.h>
#include <linux/skbuff.h>
#include <linux/spinlock_types.h>
#include <linux/types.h>

#define SNULL_STATUS_RX_INTR 0x0001
#define SNULL_STATUS_TX_INTR 0x0002

typedef void (*snull_int_t)(int irq, void *devptr, struct regs *regs);

struct snull_packet {
  struct snull_packet *next;
  struct net_device *dev;
  int len;
  u8 data[ETH_DATA_LEN];
};

struct snull_priv {
  struct net_device *dev;
  struct napi_struct napi;
  struct net_device_stats stats;
  struct sk_buff *skb;
  struct snull_packet *tx_queue;
  struct snull_packet *rx_queue;
  int rx_int_enabled;
  int tx_packet_len;
  u8 *tx_packet_data;
  int status;
  int is_registered;
  snull_int_t int_handler;
  spinlock_t lock;
};

#endif